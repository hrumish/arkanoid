﻿using ClassLibrary.Entities;
using System;

namespace ClassLibrary.Services
{
    public class GenerateLevelsService
    {
        public void Generate(Map map)
        {
            Random r = new Random();
            for (int i = 3; i < Map.mapHeight / 2; i++)
            {
                for (int j = 1; j < Map.mapWidth - 1; j += 2)
                {
                    int currPlatform = r.Next(1, 5);
                    map.map[i, j] = currPlatform;
                }
            }
        }
    }
}
