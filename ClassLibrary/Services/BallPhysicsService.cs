﻿using ClassLibrary.Entities;

namespace ClassLibrary.Services
{
    public class BallPhysicsService
    {
        public delegate void UpdateScoreDelegate(int score);
        public bool IsCollide(Player player, Ball ball, Map map, UpdateScoreDelegate updateScore)
        {
            bool isColliding = false;

            if (ball.BodyX / 20 + ball.DirX > Map.mapWidth - 2 || ball.BodyX / 20 + ball.DirX < 0)
            {
                ball.DirX *= -1;
                isColliding = true;
            }

            if (ball.BodyY / 20 + ball.DirY < 2)
            {
                ball.DirY *= -1;
                isColliding = true;
            }

            if (map.map[ball.BodyY / 20 + ball.DirY, ball.BodyX / 20] != 0)
            {
                bool addScore = false;
                isColliding = true;

                if (map.map[ball.BodyY / 20 + ball.DirY, ball.BodyX / 20] > 10 && map.map[ball.BodyY / 20 + ball.DirY, ball.BodyX / 20] < 99)
                {
                    map.map[ball.BodyY / 20 + ball.DirY, ball.BodyX / 20] = 0;
                    map.map[ball.BodyY / 20 + ball.DirY, ball.BodyX / 20 - 1] = 0;
                    addScore = true;
                }

                else if (map.map[ball.BodyY / 20 + ball.DirY, ball.BodyX / 20] < 9)
                {
                    map.map[ball.BodyY / 20 + ball.DirY, ball.BodyX / 20] = 0;
                    map.map[ball.BodyY / 20 + ball.DirY, ball.BodyX / 20 + 1] = 0;
                    addScore = true;
                }
                if (addScore)
                {
                    player.Score += 50;
                    if (player.Score % 450 == 0 && player.Score > 0)
                        map.AddBlocks();
                }
                ball.DirY *= -1;
            }
            if (map.map[ball.BodyY / 20, ball.BodyX / 20 + ball.DirX] != 0)
            {
                bool addScore = false;
                isColliding = true;

                if (map.map[ball.BodyY / 20, ball.BodyX / 20 + ball.DirX] > 10 && map.map[ball.BodyY / 20 + ball.DirY, ball.BodyX / 20] < 99)
                {
                    map.map[ball.BodyY / 20, ball.BodyX / 20 + ball.DirX] = 0;
                    map.map[ball.BodyY / 20, ball.BodyX / 20 + ball.DirX - 1] = 0;
                    addScore = true;
                }
                else if (map.map[ball.BodyY / 20, ball.BodyX / 20 + ball.DirX] < 9)
                {
                    map.map[ball.BodyY / 20, ball.BodyX / 20 + ball.DirX] = 0;
                    map.map[ball.BodyY / 20, ball.BodyX / 20 + ball.DirX + 1] = 0;
                    addScore = true;
                }
                if (addScore)
                {
                    player.Score += 50;
                    if (player.Score % 450 == 0 && player.Score > 0)
                        map.AddBlocks();
                }
                ball.DirX *= -1;
            }
            updateScore(player.Score);

            return isColliding;
        }
    }
}
