﻿using System.Drawing;
using System.IO;

namespace ClassLibrary.Entities
{
    public class Ball : GameObject
    {
        public int DirX { get; set; }
        public int DirY { get; set; }
        public int Speed { get; set; }
        public Ball()
        {
            string absolutePath = Path.GetFullPath(@"Assets");
            ObjectImage = new Bitmap(absolutePath + @"\ball.png");
        }
        public Ball(int speed) : this()
        {
            Speed = speed;
        }
    }
}
