﻿using System;
using System.Drawing;
using System.IO;

namespace ClassLibrary.Entities
{
    public class Map : GameObject
    {
        public const int mapWidth = 20;
        public const int mapHeight = 30;

        public int[,] map = new int[mapHeight, mapWidth];
        public void AddBlocks()
        {
            for (int i = mapHeight - 1; i > 0; i--)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    map[i, j] = map[i - 1, j];
                }
            }

            Random r = new Random();
            for (int j = 1; j < mapWidth - 1; j += 2)
            {
                int currPlatform = r.Next(1, 5);
                map[3, j] = currPlatform;
            }
        }

        public Map()
        {
            string absolutePath = Path.GetFullPath(@"Assets");
            ObjectImage = new Bitmap(absolutePath + @"\background.png");
        }
        public override void DrawObject(Graphics g, Size size, int[] coords)
        {
            g.DrawImage(ObjectImage, new Rectangle(new Point(BodyX + 8, BodyY + 40), size), coords[0], coords[1], coords[2], coords[3], GraphicsUnit.Pixel);
        }
    }
}
