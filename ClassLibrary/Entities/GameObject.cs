﻿using System.Drawing;

namespace ClassLibrary.Entities
{
    public class GameObject
    {
        public int BodyX { get; set; }
        public int BodyY { get; set; }
        public Image ObjectImage { get; set; }
        public virtual void DrawObject(Graphics g, Size size, int [] coords)
        {
            g.DrawImage(ObjectImage, new Rectangle(new Point(BodyX, BodyY), size), coords[0], coords[1], coords[2], coords[3], GraphicsUnit.Pixel);
        }

    }
}
