﻿using System.Drawing;
using System.IO;

namespace ClassLibrary.Entities
{
    public class Player : GameObject
    {
        public int BestScore { get; set; }
        public int Score { get; set; }
        public int Speed { get; set; }
        public int Lives { get; set; }
        public Player()
        {
            string absolutePath = Path.GetFullPath(@"Assets");
            ObjectImage = new Bitmap(absolutePath + @"\platforms.png");
        }
        public Player(int score, int lives, int speed, int bestScore) : this()
        {
            Speed = speed;
            Score = score;
            Lives = lives;
            BestScore = bestScore;
        }
    }
}
