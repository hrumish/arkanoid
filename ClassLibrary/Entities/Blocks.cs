﻿using System.Drawing;
using System.IO;

namespace ClassLibrary.Entities
{
    public class Blocks
    {
        public Image BlocksImage { get; private set; }
        public Blocks()
        {
            string absolutePath = Path.GetFullPath(@"Assets");
            BlocksImage = new Bitmap(absolutePath + @"\blocks.png");
        }
        public void DrawBlocks(Graphics g, Map map)
        {
            for (int i = 0; i < Map.mapHeight; i++)
            {
                for (int j = 0; j < Map.mapWidth; j++)
                {
                    switch (map.map[i, j])
                    {
                        case 1:
                            g.DrawImage(BlocksImage, new Rectangle(new Point(j * 20, i * 20), new Size(40, 20)), 0, 0, 15, 6, GraphicsUnit.Pixel);
                            break;
                        case 2:
                            g.DrawImage(BlocksImage, new Rectangle(new Point(j * 20, i * 20), new Size(40, 20)), 0 + 16, 0, 15, 6, GraphicsUnit.Pixel);
                            break;
                        case 3:
                            g.DrawImage(BlocksImage, new Rectangle(new Point(j * 20, i * 20), new Size(40, 20)), 0, 8, 15, 6, GraphicsUnit.Pixel);
                            break;
                        case 4:
                            g.DrawImage(BlocksImage, new Rectangle(new Point(j * 20, i * 20), new Size(40, 20)), 0 + 16, 8, 15, 6, GraphicsUnit.Pixel);
                            break;
                        default: break;
                    }
                }
            }
        }
    }
}
