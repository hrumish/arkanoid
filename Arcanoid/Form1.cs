﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using ClassLibrary.Entities;
using ClassLibrary.Services;
using System.Media;

namespace Arcanoid
{
    public partial class Form1 : Form
    {
        Map map;
        Player player;
        Ball ball;
        BallPhysicsService ballPhysics;
        readonly GenerateLevelsService generateLevel;
        Blocks block;

        readonly SoundPlayer MainMusic;

        public Label scoreLabel;
        public Label livesLabel;
        public Label bestScoreLabel;

        private bool ballMoving;
        private readonly string absolutePath;
        private readonly string bestScoreFilePath;
        private readonly int bestScore;

        public Form1()
        {
            InitializeComponent();
            absolutePath = Path.GetFullPath(@"Assets");
            MainMusic = new SoundPlayer(absolutePath + @"\music.wav");
            bestScoreFilePath = (absolutePath + @"\best_score.txt");
            bestScore = LoadBestScore();
            generateLevel = new GenerateLevelsService();
            //MainMusic.Play();
        }

        private void buttonToStart_Click(object sender, EventArgs e)
        {
            buttonToStart.Visible = false;
            buttonToExit.Visible = false;
            InitializeGame();
            this.Paint += new PaintEventHandler(this.OnPaint);
            this.Focus();
        }

        private void InitializeGame()
        { 
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;

            scoreLabel = new Label
            {
                Location = new Point((Map.mapWidth), 10),
                Font = new Font("Bernard MT Condensed", 14),
                ForeColor = Color.Gold,
                AutoSize = true
            };

            livesLabel = new Label
            {
                Location = new Point((Map.mapWidth) * 6, 10),
                Font = new Font("Bernard MT Condensed", 14),
                ForeColor = Color.Gold,
                AutoSize = true
            };


            bestScoreLabel = new Label
            {
                Location = new Point((Map.mapWidth) * 13, 10),
                Font = new Font("Bernard MT Condensed", 14),
                ForeColor = Color.Gold,
                AutoSize = true
            };

            this.Controls.Add(scoreLabel);
            this.Controls.Add(livesLabel);
            this.Controls.Add(bestScoreLabel);

            timer1.Tick += new EventHandler(updateGame);
            this.KeyDown += new KeyEventHandler(CheckKey);

            Init();
        }

        private void CheckKey(object sender, KeyEventArgs e)
        {
            map.map[player.BodyY / 20, player.BodyX / 20] = 0;
            map.map[player.BodyY / 20, player.BodyX / 20 + 1] = 0;
            map.map[player.BodyY / 20, player.BodyX / 20 + 2] = 0;
            switch (e.KeyCode)
            {
                case Keys.Right:
                    if (player.BodyX / 20 + 3 < Map.mapWidth - 1)
                        player.BodyX += player.Speed;
                    break;
                case Keys.Left:
                    if (player.BodyX > 25)
                        player.BodyX -= player.Speed;
                    break;
            }
            map.map[player.BodyY / 20, player.BodyX / 20] = 9;
            map.map[player.BodyY / 20, player.BodyX / 20 + 1] = 99;
            map.map[player.BodyY / 20, player.BodyX / 20 + 2] = 999;

            ballMoving = true;
        }

        private void updateGame(object sender, EventArgs e)
        {
            if (ball.BodyY / 20 + ball.DirY > Map.mapHeight - 1)
            {
                player.Lives--;
                if (player.Lives == 0)
                {
                    if (player.Score > player.BestScore)
                    {
                        player.BestScore = player.Score;
                        SaveBestScore(player.BestScore);
                    }
                    GameOver();
                }
                else
                    ContinueGame();
            }

            map.map[ball.BodyY / 20, ball.BodyX / 20] = 0;

            if (ballMoving)
            {
                if (!ballPhysics.IsCollide(player, ball, map, UpdateScore))
                    ball.BodyX += ball.DirX * ball.Speed;
                if (!ballPhysics.IsCollide(player, ball, map, UpdateScore))
                    ball.BodyY += ball.DirY * ball.Speed;
            }
            map.map[ball.BodyY / 20, ball.BodyX / 20] = 8;

            map.map[player.BodyY / 20, player.BodyX / 20] = 9;
            map.map[player.BodyY / 20, player.BodyX / 20 + 1] = 99;
            map.map[player.BodyY / 20, player.BodyX / 20 + 2] = 999;

            Invalidate();
        }

        private void GameOver()
        {
            timer1.Stop();
            MessageBox.Show("Game Over!");
            Application.Exit();
            Init();
        }

        private void UpdateScore(int score)
        {
            scoreLabel.Text = "Score: " + score;
        }

        private int LoadBestScore()
        {
            if (File.Exists(bestScoreFilePath))
            {
                string scoreText = File.ReadAllText(bestScoreFilePath);
                if (int.TryParse(scoreText, out int bestScore))
                    return bestScore;
            }
            return 0;
        }

        private void SaveBestScore(int score)
        {
            File.WriteAllText(bestScoreFilePath, score.ToString());
        }

        public void ContinueGame()
        {
            timer1.Interval = 1;
            scoreLabel.Text = "Score: " + player.Score;
            livesLabel.Text = "Lives: " + player.Lives;
            bestScoreLabel.Text = "High Score: " + player.BestScore;

            map.map[player.BodyY / 20, player.BodyX / 20] = 9;
            map.map[player.BodyY / 20, player.BodyX / 20 + 1] = 99;
            map.map[player.BodyY / 20, player.BodyX / 20 + 2] = 999;
            map.map[ball.BodyY / 20, ball.BodyX / 20] = 0;

            ball.BodyY = player.BodyY - 20;
            ball.BodyX = player.BodyX + 20;

            map.map[ball.BodyY / 20, ball.BodyX / 20] = 8;

            ball.DirX = 1;
            ball.DirY = -1;

            ballMoving = false;

            timer1.Start();
        }

        public void Init()
        {
            player = new Player(0, 5, 4, bestScore);
            map = new Map();
            block = new Blocks();
            ball = new Ball(3);
            ballPhysics = new BallPhysicsService();

            this.Width = (Map.mapWidth + 1) * 20;
            this.Height = (Map.mapHeight + 2) * 20;

            timer1.Interval = 10;

            scoreLabel.Text = "Score: " + player.Score;
            livesLabel.Text = "Lives: " + player.Lives;
            bestScoreLabel.Text = "High Score: " + player.BestScore;
            for (int i = 0; i < Map.mapHeight; i++)
            {
                for (int j = 0; j < Map.mapWidth; j++)
                    map.map[i, j] = 0;
            }

            player.BodyX = (Map.mapWidth - 1) / 2 * 20;
            player.BodyY = (Map.mapHeight - 1) * 20;

            map.map[player.BodyY / 20, player.BodyX / 20] = 9;
            map.map[player.BodyY / 20, player.BodyX / 20 + 1] = 99;
            map.map[player.BodyY / 20, player.BodyX / 20 + 2] = 999;

            ball.BodyY = player.BodyY - 20;
            ball.BodyX = player.BodyX + 20;

            map.map[ball.BodyY / 20, ball.BodyX / 20] = 8;

            ball.DirX = 1;
            ball.DirY = -1;

            ballMoving = false;

            generateLevel.Generate(map);

            timer1.Start();
        }
        private void buttonToExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OnPaint(object sender, PaintEventArgs e)
        {
            map.DrawObject(e.Graphics, new Size(432, 585), new int[] { 0, 0, 250, 250 });
            ball.DrawObject(e.Graphics, new Size(20,20), new int[] { 806, 548, 73, 73 });
            player.DrawObject(e.Graphics, new Size (60,20), new int[] { 0, 0, 31, 7 });
            block.DrawBlocks(e.Graphics, map);
        }
    }
}
