﻿using System.Windows.Forms;

namespace Arcanoid
{
    partial class Form1
    {
        private System.ComponentModel.IContainer components = null;

        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonToStart = new System.Windows.Forms.Button();
            this.buttonToExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonToStart
            // 
            this.buttonToStart.BackColor = System.Drawing.Color.Black;
            this.buttonToStart.Font = new System.Drawing.Font("Bernard MT Condensed", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonToStart.ForeColor = System.Drawing.Color.Gold;
            this.buttonToStart.Location = new System.Drawing.Point(120, 200);
            this.buttonToStart.Name = "buttonToStart";
            this.buttonToStart.Size = new System.Drawing.Size(300, 85);
            this.buttonToStart.TabIndex = 1;
            this.buttonToStart.Text = "Start";
            this.buttonToStart.UseVisualStyleBackColor = false;
            this.buttonToStart.Click += new System.EventHandler(this.buttonToStart_Click);
            // 
            // buttonToExit
            // 
            this.buttonToExit.BackColor = System.Drawing.Color.Black;
            this.buttonToExit.Font = new System.Drawing.Font("Bernard MT Condensed", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonToExit.ForeColor = System.Drawing.Color.Gold;
            this.buttonToExit.Location = new System.Drawing.Point(120, 375);
            this.buttonToExit.Name = "buttonToExit";
            this.buttonToExit.Size = new System.Drawing.Size(300, 85);
            this.buttonToExit.TabIndex = 2;
            this.buttonToExit.Text = "Exit";
            this.buttonToExit.UseVisualStyleBackColor = false;
            this.buttonToExit.Click += new System.EventHandler(this.buttonToExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(532, 733);
            this.Controls.Add(this.buttonToExit);
            this.Controls.Add(this.buttonToStart);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Arcanoid";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CheckKey);
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button buttonToStart;
        private System.Windows.Forms.Button buttonToExit;
    }
}

